from django.conf import settings
from django.urls import re_path
from django.contrib import admin
from django.urls import include, path

from django_pwmgr_api.django_pwmgr_api import urls as api_urls

urlpatterns = [
    re_path(r"", include("user_sessions.urls", "user_sessions")),
    path("admin/", admin.site.urls),
    re_path(r"^watchman/", include("watchman.urls")),
    path("api/", include(api_urls)),
    path("otp/import", include("django_pwmgr_otp_import.django_pwmgr_otp_import.urls")),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
